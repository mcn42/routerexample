/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 *
 * @author 00827843
 */
public class CommunicationErrorTracker {
    private long startedAt = -1;
    
    private final AtomicReference<Map<Long, String>> errorsByTime = new AtomicReference(new HashMap<>());
    private final EntityKey key;

    public CommunicationErrorTracker(EntityKey key) {
        this.key = key;
    }
    
    public void begin() {
        this.errorsByTime.get().clear();
        this.startedAt = System.currentTimeMillis();
    }
    
    private final float MILLIS_PER_HOUR = TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS) * 1.0f;
    public String getReport() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Exceptions for %s", this.key)).append("\n");
        long millis = System.currentTimeMillis() - this.startedAt;
        float hours = millis / MILLIS_PER_HOUR;
        int errCount = this.errorsByTime.get().size();
        float rate = hours == 0?errCount:errCount / (hours * 1.0f);
        
        sb.append(String.format("Running for %.2f hours", hours)).append("\n");
        sb.append(String.format("%d reported exceptions", errCount)).append("\n");
        sb.append(String.format("Error rate = %.2f", rate)).append("\n");
        
        return sb.toString();
    }
    
    public synchronized void reportException(Throwable t) {
        Long time = System.currentTimeMillis();
        if(this.errorsByTime.get().containsKey(time)) time++;
        this.errorsByTime.get().put(time , t.getLocalizedMessage());
        Log.logger().info(this.getReport());
    }
    
    public synchronized void reportMessage(String msg) {
        Long time = System.currentTimeMillis();
        if(this.errorsByTime.get().containsKey(time)) time++;
        this.errorsByTime.get().put(time , msg);
        Log.logger().info(this.getReport());
    }
}
