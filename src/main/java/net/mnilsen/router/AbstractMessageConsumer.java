/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author 00827843
 * @param <T> - The payload/result class for the Consumer
 */
public abstract class AbstractMessageConsumer<T extends Object> {
    protected final String listenHost;
    protected final int listenPort;
    protected EntityKey key;
    protected AsynchronousServerSocketChannel channel;
    protected final List<IncomingMessageListener> messageListeners = new ArrayList<>();
    protected CommunicationErrorTracker errorTracker = null;

    public AbstractMessageConsumer() {
        this.listenHost = "127.0.0.1";
        this.listenPort = 5454;
    }
    
    public AbstractMessageConsumer(String listenHost, int listenPort) {
        this.listenHost = listenHost;
        this.listenPort = listenPort;
    }
    
    public void createAndStart(AsynchronousChannelGroup group, Object a, EntityKey key) throws SetupException {
        try {
            this.key = key;
            errorTracker = new CommunicationErrorTracker(key);
            errorTracker.begin();
            InetSocketAddress addr = new InetSocketAddress(this.listenHost,this.listenPort);
            this.channel = AsynchronousServerSocketChannel.open(group);
            this.channel.bind(addr);
            this.channel.accept(a, this.getCompletionHandler()); 
            Log.logger().info(String.format("%s accepting connections on %s:%s", this.key,this.listenHost, this.listenPort));
        } catch (IOException ex) {
            String msg = String.format("IO Error on createAndStart: Host: %s, Port %d", this.listenHost, this.listenPort);
            Log.logger().log(Level.SEVERE, msg, ex);
            throw new SetupException(msg,ex);
        }
    }

    public String getListenHost() {
        return listenHost;
    }

    public int getListenPort() {
        return listenPort;
    }

    public EntityKey getKey() {
        return key;
    }

    public AsynchronousServerSocketChannel getChannel() {
        return channel;
    }

    public CommunicationErrorTracker getErrorTracker() {
        return errorTracker;
    }
    
    public void addIncomingMessageListener(IncomingMessageListener listener) {
        this.messageListeners.add(listener);
    }
    
    public abstract CompletionHandler getCompletionHandler();
    public abstract void setCompletionHandler(CompletionHandler handler);
}
