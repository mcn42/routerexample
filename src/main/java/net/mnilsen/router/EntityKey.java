/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.util.Objects;

/**
 *
 * @author 00827843
 */
public class EntityKey {
    private final int key;      //  Globally unique integer ID
    private final String name;
    private final int sequence; //  Distinguishes multiple connections for the same EntityType
    private final EntityType type;

    public EntityKey(int key, String name, int sequence, EntityType type) {
        this.key = key;
        this.name = name;
        this.sequence = sequence;
        this.type = type;
    }

    public int getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public int getSequence() {
        return sequence;
    }

    public EntityType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + this.key;
        hash = 43 * hash + Objects.hashCode(this.name);
        hash = 43 * hash + this.sequence;
        hash = 43 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityKey other = (EntityKey) obj;
        if (this.key != other.key) {
            return false;
        }
        if (this.sequence != other.sequence) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return this.type == other.type;
    }
    private static final String TO_STRING_FORMAT = "%s: %s - %d/%d";
    
    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT, this.type, this.name, this.key, this.sequence);
    }
}
