/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.io.IOException;
import java.nio.channels.AsynchronousChannelGroup;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Server -> Listens for incoming messages from "remote" systems -> Message Consumer
 *  Client -> Sends messages to "remote" systems -> Message Producer
 * 
 * @author 00827843
 */

/**  */
public class ConnectionManager {
    private final AtomicInteger idGenerator = new AtomicInteger(-1);
    private final AtomicReference<Map<EntityKey,AbstractMessageProducer>> clients = new AtomicReference<>(new HashMap<>());
    private final AtomicReference<Map<EntityKey,AbstractMessageConsumer>> servers = new AtomicReference<>(new HashMap<>());
    
    private ConfigurationSource configSource = null;
    
    private ExecutorService serverExecutor = null;
    private ExecutorService clientExecutor = null;
    
    private AsynchronousChannelGroup serverGroup = null;
    private AsynchronousChannelGroup clientGroup = null;

    public ConnectionManager(ConfigurationSource configSource) throws SetupException {
        this.configSource = configSource;
        initialize();
    }
    
    public final void initialize() throws SetupException {
        int cores = Runtime.getRuntime().availableProcessors();
        clientExecutor = new ThreadPoolExecutor(cores, cores * 2, 120, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(cores * 2));
        serverExecutor = new ThreadPoolExecutor(cores, cores * 2, 120, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(cores * 2));
        try {
            this.clientGroup = AsynchronousChannelGroup.withThreadPool(clientExecutor);
            this.serverGroup = AsynchronousChannelGroup.withThreadPool(serverExecutor);
            
//            this.addServers(this.configSource.getConsumerSet(), EntityType.ESB_CONSUMER, this.configSource.getBaseName());
//            Thread.sleep(10000);
//            this.addClients(this.configSource.getProducerSet(), EntityType.ESB_PRODUCER, this.configSource.getBaseName());
            
        } catch (IOException ex) {
            String msg = "IO Error during ConnectionManager initialization";
            Log.logger().log(Level.SEVERE, msg, ex);
            throw new SetupException(msg,ex);
        }
    }
    
    public Set<EntityKey> addServers(List<AbstractMessageConsumer> consumers, EntityType type, String baseName) throws SetupException {
        HashSet<EntityKey> keys = new HashSet<>();
        for(int i = 0; i < consumers.size(); i++) {
            EntityKey key = new EntityKey(this.idGenerator.incrementAndGet(), baseName, i, type);
            AbstractMessageConsumer con = consumers.get(i);
            con.createAndStart(serverGroup, null, key);
            this.servers.get().put(key,con);
            keys.add(key);
        }
        return keys;
    }
    
    public Set<EntityKey> addClients(List<AbstractMessageProducer> producers, EntityType type, String baseName) {
        HashSet<EntityKey> keys = new HashSet<>();
        for(int i = 0; i < producers.size(); i++) {
            EntityKey key = new EntityKey(this.idGenerator.incrementAndGet(), baseName, i, type);
            AbstractMessageProducer prod = producers.get(i);
            try {
                prod.createAndStart(clientGroup, key);
            } catch (SetupException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.clients.get().put(key, prod);
            keys.add(key);
        }
        return keys;
    }
    
    public synchronized AbstractMessageProducer getClient(EntityKey key) {
        return this.clients.get().get(key);
    }
    
    public synchronized AbstractMessageConsumer getServer(EntityKey key) {
        return this.servers.get().get(key);
    }
    
    public synchronized void testAllConnections() {
    }
    
}
