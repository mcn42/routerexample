/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 *
 * @author 00827843
 */
public class XmlMessageProducer extends AbstractMessageProducer<String> {
    private Random r = new Random();
    public XmlMessageProducer(String remoteHost, int remotePort) {
        super(remoteHost, remotePort);
    }
    
    @Override
    public void sendMessage(String message) throws CommunicationException {
        ByteBuffer bytes = ByteBuffer.wrap(message.getBytes(Charset.forName("UTF-8")));
        if(r.nextInt(20) == 0) {
            errorTracker.reportMessage("Random error");
            throw new CommunicationException("Random error");
        }
        this.channel.write(bytes, 100, TimeUnit.SECONDS, null,new XmlCompletionHandler());
    }
    
    class XmlCompletionHandler implements CompletionHandler<Integer, Object> {

        @Override
        public void completed(Integer result, Object attachment) {
            Log.logger().info(String.format("%s successfully sent %d bytes to %s", key,result, remoteAddress));
        }

        @Override
        public void failed(Throwable exc, Object attachment) {
            String msg = String.format("Message send error: %s", key);
            Log.logger().log(Level.SEVERE, msg, exc);
            errorTracker.reportException(exc);
        }
        
    } 
    
}
