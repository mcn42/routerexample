/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.io.Serializable;

/**
 *
 * @author 00827843
 */
public class IncomingMessageEvent implements Serializable {
    private String messageText;
    private long received;

    public IncomingMessageEvent() {
    }

    public IncomingMessageEvent(String messageText, long received) {
        this.messageText = messageText;
        this.received = received;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public long getReceived() {
        return received;
    }

    public void setReceived(long received) {
        this.received = received;
    }
    
}
