/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package net.mnilsen.router;

/**
 *
 * @author 00827843
 */
public interface IncomingMessageListener {
    void handleIncomingMessage(IncomingMessageEvent event);
}
