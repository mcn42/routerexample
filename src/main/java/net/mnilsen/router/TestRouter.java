/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michaeln
 */
public class TestRouter implements IncomingMessageListener {

    private ConfigurationSource config = null;
    private ConnectionManager manager = null;
    private EntityKey[] servers = null;
    private EntityKey[] clients = null;

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final AtomicLong messageCounter = new AtomicLong(0L);

    public TestRouter(ConfigurationSource configurationSource) {
        this.config = configurationSource;
        try {
            this.manager = new ConnectionManager(configurationSource);
        } catch (SetupException ex) {
            Logger.getLogger(TestRouter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void start(long delaySecs) {
        if (delaySecs <= 0) {
            run();
        } else {
            try {
                Thread.sleep(delaySecs * 1000);
            } catch (InterruptedException ex) {
                //  no op
            }
            run();
        }
    }

    private void run() {
        try {
            Set<EntityKey> serverKeys = manager.addServers(config.getConsumerSet(), EntityType.RDT_CONSUMER, config.getBaseName());
            Set<EntityKey> clientKeys = manager.addClients(config.getProducerSet(), EntityType.RDT_PRODUCER, config.getBaseName());

            this.servers = new EntityKey[serverKeys.size()];
            Log.logger().info(String.format("%s: Added %d servers", config.getBaseName(), serverKeys.size()));
            serverKeys.toArray(servers);

            Thread.sleep(10000);

            this.clients = new EntityKey[clientKeys.size()];
            Log.logger().info(String.format("%s: Added %d clients", config.getBaseName(), clientKeys.size()));
            clientKeys.toArray(clients);

            this.manager.getServer(servers[0]).addIncomingMessageListener(this);
            this.running.set(true);

            Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
                this.sendMessage();
            }, 1L, 1L, TimeUnit.SECONDS);

        } catch (SetupException | InterruptedException ex) {
            Log.logger().log(Level.SEVERE, "Setup failure, router will exit...", ex);
            System.exit(1);
        }
    }

    public synchronized void stop() {
        this.running.set(false);
    }

    public void sendMessage() {
        try {
            String time = timeFormat.format(System.currentTimeMillis());
            long msgId = this.messageCounter.incrementAndGet();
            String message = String.format("Message from %s at %s, ID=%d", config.getBaseName(), time, msgId);
            Log.logger().info(String.format("%s: Sending message at %s:\n%s", config.getBaseName(), time, message));

            this.manager.getClient(this.clients[0]).sendMessage(message);
        } catch (Exception ex) {
            Logger.getLogger(TestRouter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss:SSS");

    @Override
    public void handleIncomingMessage(IncomingMessageEvent event) {
        String time = timeFormat.format(event.getReceived());
        Log.logger().info(String.format("%s: Message received at %s:\n%s", config.getBaseName(), time, event.getMessageText()));
    }

}
