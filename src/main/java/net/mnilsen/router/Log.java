/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.util.logging.Logger;


/**
 *
 * @author 00827843
 */
public class Log {
    private static final Logger logger = Logger.getLogger("net.mnilsen.router");
    
    static {
       configureLog(); 
    }
    
    private static void configureLog() {
        
    }
    
    public static Logger logger() {
        return logger;
    }
}
