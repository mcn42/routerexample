/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 00827843
 */
public class ConfigurationSourceTwo implements ConfigurationSource {
    
    @Override
    public List<AbstractMessageProducer> getProducerSet() {
        List<AbstractMessageProducer> producers = new ArrayList();
        producers.add(new XmlMessageProducer("localhost",5455));
        return producers;
    }
    
    @Override
    public List<AbstractMessageConsumer> getConsumerSet() {
        List<AbstractMessageConsumer> consumers = new ArrayList();
        consumers.add(new XmlMessageConsumer("localhost",5454));
        return consumers;
    }
    
    @Override
    public String getBaseName() {
        return "RouterTwo";
    }
}
