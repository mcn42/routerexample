/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;

/**
 *
 * @author 00827843
 * @param <T> - The payload/result class for the Producer
 */
public abstract class AbstractMessageProducer<T extends Object> {

    protected final String remoteHost;
    protected final int remotePort;
    protected EntityKey key;
    protected AsynchronousSocketChannel channel;
    protected CommunicationErrorTracker errorTracker = null;
    InetSocketAddress remoteAddress = null;

    public AbstractMessageProducer() {
        this.remoteHost = "127.0.0.1";
        this.remotePort = 5454;
        remoteAddress = new InetSocketAddress(this.remoteHost, this.remotePort);
    }

    public AbstractMessageProducer(String remoteHost, int remotePort) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        remoteAddress = new InetSocketAddress(this.remoteHost, this.remotePort);
    }

    public void createAndStart(AsynchronousChannelGroup group, EntityKey key) throws SetupException {
        try {
            this.key = key;
            errorTracker = new CommunicationErrorTracker(key);
            errorTracker.begin();
            this.channel = AsynchronousSocketChannel.open(group);
            boolean connected = this.connect();
            Log.logger().info(String.format("%s connected to remote server %s: %s",
              this.key, this.remoteAddress,connected));
        } catch (IOException ex) {
            String msg = String.format("IO Error on createAndStart: Host: %s, Port %d", this.remoteHost, this.remotePort);
            Log.logger().log(Level.SEVERE, msg, ex);
            throw new SetupException(msg, ex);
        }
    }

    public boolean connect() {
        try {
            Future<Void> con = this.channel.connect(remoteAddress);
            //  blocks here:
            con.get();
            Log.logger().info(String.format("%s connected to remote server %s", this.key, this.remoteAddress));
            return this.channel.isOpen();
        } catch (InterruptedException | ExecutionException ex) {
            String msg = String.format("Error on connect: Host: %s, Port %d", this.remoteHost, this.remotePort);
            Log.logger().log(Level.SEVERE, msg, ex);
            errorTracker.reportException(ex);
            return false;
        }
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public EntityKey getKey() {
        return key;
    }

    public AsynchronousSocketChannel getChannel() {
        return channel;
    }

    public CommunicationErrorTracker getErrorTracker() {
        return errorTracker;
    }

    public abstract void sendMessage(T message) throws CommunicationException;
}
