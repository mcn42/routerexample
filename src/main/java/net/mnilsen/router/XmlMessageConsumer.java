/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;

/**
 *
 * @author 00827843
 */
public class XmlMessageConsumer extends AbstractMessageConsumer<String> {

    private static final int BUFFER_SIZE = 1024 * 64;

    private XmlCompletionHandler handler = new XmlCompletionHandler();
    private Random r = new Random();

    public XmlMessageConsumer(String listenHost, int listenPort) {
        super(listenHost, listenPort);
    }

    @Override
    public CompletionHandler getCompletionHandler() {
        return handler;
    }

    @Override
    public void setCompletionHandler(CompletionHandler handler) {
        this.handler = (XmlCompletionHandler) handler;
    }

    class XmlCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {

        @Override
        public void completed(AsynchronousSocketChannel result, Object attachment) {
            //  Reset server for next message
            if (channel.isOpen()) {
                channel.accept(attachment, this);
            } else {
                String msg = String.format("Could not reset channel: %s", key);
                Log.logger().log(Level.SEVERE, msg);
                errorTracker.reportMessage(msg);
            }

            StringBuilder sb = new StringBuilder();
            long bytesRead = 0L;
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            if (result != null && result.isOpen()) {
                try {
                    Future<Integer> future = result.read(buffer);
                    //  blocks here:
                    int bts = future.get();
                    while (bts != -1) {
                        bytesRead += bts;
                        String part = new String(buffer.array(), Charset.forName("UTF-8"));
                        sb.append(part);

                        future = result.read(buffer);
                        if (r.nextInt(20) == 0) {
                            errorTracker.reportMessage("Random receive error");
                            throw new ExecutionException(new IOException("Random receive error"));
                        }
                        //  blocks here:
                        bts = future.get();
                    }
                    Log.logger().info(String.format("Received message of %d bytes", bytesRead));
                } catch (InterruptedException | ExecutionException ex) {
                    String msg = String.format("Message receipt error: %s", key);
                    Log.logger().log(Level.SEVERE, msg, ex);
                    errorTracker.reportException(ex);
                }

                //  Fire message to listeners
                IncomingMessageEvent event = new IncomingMessageEvent(sb.toString(), System.currentTimeMillis());
                messageListeners.forEach((l) -> {
                    l.handleIncomingMessage(event);
                });
            }
        }

        @Override
        public void failed(Throwable exc, Object attachment) {
            String msg = String.format("Message receipt error: %s", key);
            Log.logger().log(Level.SEVERE, msg, exc);
            errorTracker.reportException(exc);
        }

    }

}
