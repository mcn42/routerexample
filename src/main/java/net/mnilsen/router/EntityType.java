/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package net.mnilsen.router;

/**
 *
 * @author 00827843
 */
public enum EntityType {
    RDT_CONSUMER(false), RDT_PRODUCER(true), 
      ESB_CONSUMER(false), ESB_PRODUCER(true), 
      NJT_CONSUMER(false), NJT_PRODUCER(true);
    
    private final boolean producer;

    private EntityType(boolean producer) {
        this.producer = producer;
    }

    public boolean isProducer() {
        return producer;
    }
    
    public boolean isConsumer() {
        return !producer;
    }
    
}
