/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package net.mnilsen.router;

import java.util.List;

/**
 *
 * @author 00827843
 */
public interface ConfigurationSource {

    String getBaseName();

    List<AbstractMessageConsumer> getConsumerSet();

    List<AbstractMessageProducer> getProducerSet();
    
}
