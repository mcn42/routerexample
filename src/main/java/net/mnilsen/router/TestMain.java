/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.router;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 * @author michaeln
 */
public class TestMain {
    private static TestRouter routerOne = new TestRouter(new ConfigurationSourceOne());
    private static TestRouter routerTwo = new TestRouter(new ConfigurationSourceTwo());
    
    private static Executor exec = Executors.newFixedThreadPool(2);
    
    public static void main(String[] atgs) {
        exec.execute(() -> {routerOne.start(0);});
        exec.execute(() -> {routerTwo.start(0);});
        
    }
}
